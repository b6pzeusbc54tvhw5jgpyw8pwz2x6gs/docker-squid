# docker-squid

## Generate username & passowrd

```
docker run --rm --entrypoint htpasswd httpd:alpine -bn <username> <password> >> ./passwords
```

If you want to use auth, modify `docker-compose.yml` like below:
```diff
     volumes:
       - ./cache:/var/spool/squid3
-      - ./squid.allallow.conf:/etc/squid3/squid.conf:ro
-      #- ./squid.useauth.conf:/etc/squid3/squid.conf:ro
-      #- ./passwords:/etc/squid3/passwords:ro
+      #- ./squid.allallow.conf:/etc/squid3/squid.conf:ro
+      - ./squid.useauth.conf:/etc/squid3/squid.conf:ro
+      - ./passwords:/etc/squid3/passwords:ro
```


## squid.custom.conf
refer to `squid2` in `docker-compose.yml`

```bash
# allow specific ip sources
acl gas src 123.123.123.123/32
acl gas src 123.123.123.124/32
http_access allow gas

# Make sure your custom config is before the "deny all" line
http_access deny all

http_port 3128
```


## View log

```
$ docker-compose exec squid tail -f /var/log/squid3/access.log
```
